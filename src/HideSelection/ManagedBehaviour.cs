﻿using System.Collections.Generic;
using UnityEngine;

namespace HideSelection {

    /// <summary>
    /// Provides centrally-managed update loops for MonoBehaviour classes. 
    /// This is apparently 3x faster than using Update() directly with 10k objects lying around: https://blog.unity.com/engine-platform/10000-update-calls.
    /// </summary>
    public class ManagedBehaviour : MonoBehaviour {

        public class Manager : MonoBehaviour {
            void Update() {
                while (EnableEvents.Count > 0) {
                    var e = EnableEvents.Dequeue();
                    if (e.enabled) { Behaviours.Add(e.behaviour); }
                    else { Behaviours.Remove(e.behaviour); }
                }
                foreach (ManagedBehaviour b in Behaviours) { b.OnUpdate(); }
            }
        }

        internal struct EnableEvent { public bool enabled; public ManagedBehaviour behaviour; }

        internal static HashSet<ManagedBehaviour> Behaviours = new HashSet<ManagedBehaviour>();
        internal static Queue<EnableEvent> EnableEvents = new Queue<EnableEvent>();

        private void OnEnable() {
            EnableEvents.Enqueue(new EnableEvent { enabled = true, behaviour = this });
            OnEnabled();
        }

        private void OnDisable() {
            EnableEvents.Enqueue(new EnableEvent { enabled = false, behaviour = this });
            OnDisabled();
        }

        /// <summary>
        /// Called when the behaviour/gameobject is enabled or created.
        /// </summary>
        protected virtual void OnEnabled() {
        }

        /// <summary>
        /// Called when the behaviour/gameobject is disabled or destroyed.
        /// </summary>
        protected virtual void OnDisabled() {
        }

        /// <summary>
        /// Called every frame, if the behaviour is enabled.
        /// </summary>
        protected virtual void OnUpdate() {
        }

    }
}
