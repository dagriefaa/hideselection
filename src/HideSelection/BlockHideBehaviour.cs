﻿using SRF;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace HideSelection {
    public class BlockHideBehaviour : ManagedBehaviour {

        static Dictionary<BlockSkinLoader.SkinPack.Skin, Material[]> GhostMaterials = new Dictionary<BlockSkinLoader.SkinPack.Skin, Material[]>();

        public bool IsHidden = false;

        List<Collider> Colliders = new List<Collider>();

        public BlockBehaviour block;
        BlockVisualController visualController;
        BlockSkinLoader.SkinPack.Skin lastSkin;
        Material[] localGhostMaterial = null;

        bool lastOverlayState = false;
        bool lastSkinsEnabledState = true;

        void Start() {
            if (ReferenceMaster.activeMachineSimulating) {
                if (block is BuildSurface surf) {
                    surf.simColliderParent.SetLayerRecursive(0);
                }
                Destroy(this);
                return;
            }
            ReferenceMaster.onMachineSimulation += OnMachineSimulation;

            block = GetComponent<BlockBehaviour>();
            visualController = block.VisualController;
            lastSkin = visualController.selectedSkin;

            if (block.myBounds) {
                Colliders.AddRange(block.myBounds.childColliders);
            }
            // adding points, occluders
            foreach (var c in block.GetComponentsInChildren<Collider>()) {
                if (c.gameObject.layer == 12 || c.gameObject.layer == 21 || c.gameObject.layer == 27) {
                    Colliders.Add(c);
                }
            }

            // i really hate that this isn't just automatic
            if (block is BuildSurface surface) {
                OnSurfaceCollidersChanged();

                if (!Colliders.Contains(surface.meshCollider)) {
                    Colliders.Add(surface.meshCollider);
                }

                // i really hate that this isn't just automatic
                if (surface.nodes.Length == 3) {
                    Colliders.Remove(surface.AddingPoints[3]);
                }
            }

            if (block is CogMotorControllerHinge) { // may be meshy
                Colliders.RemoveAll(c => !c.enabled);
            }
        }

        public void OnSurfaceCollidersChanged() {
            if (block is BuildSurface surface) {
                // they aren't needed for selecting in build mode so just hide them from the cursor
                surface.simColliderParent.SetLayerRecursive(2);
            }
        }

        void OnDestroy() {
            ReferenceMaster.onMachineSimulation -= OnMachineSimulation;
        }

        void OnMachineSimulation(Machine m, bool b) {
            if (m != Machine.Active()) {
                return;
            }
            if (IsHidden) {
                ChangeHiddenState(!b, false);
            }
        }

        protected override void OnUpdate() {
            if (!IsHidden) {
                return;
            }

            if (visualController.selectedSkin != lastSkin) {
                lastSkin = visualController.selectedSkin;
                localGhostMaterial = null;
                ChangeHiddenState(false);
            }

            var overlayState = StatMaster.clusterCoded || StatMaster.aeroCoded || StatMaster.stressCoded;
            if (lastOverlayState != overlayState) {
                lastOverlayState = overlayState;
                if (!overlayState) {
                    ChangeHiddenState(true);
                }
            }

            if (lastSkinsEnabledState != OptionsMaster.skinsEnabled) {
                lastSkinsEnabledState = OptionsMaster.skinsEnabled;
                ChangeHiddenState(true);
            }

            if (visualController.Block is BuildSurface & Colliders.FirstOrDefault()?.enabled ?? throw new NullReferenceException("No surface mesh collider found?")) {
                ChangeHiddenState(true);
            }
        }

        public void ChangeHiddenState(bool hidden, bool updateHiddenState = true) {
            if (updateHiddenState) {
                IsHidden = hidden;
            }
            if (!hidden) {
                visualController.SetNormal();
                if (block is BuildSurface && (block as BuildSurface).nodes.Length == 3) { // check just in case the surface was merged down
                    Colliders.Remove((block as BuildSurface).AddingPoints[3]);
                }
                foreach (var c in Colliders) {
                    c.enabled = true;
                }
            }
            else {
                if (localGhostMaterial == null && !GhostMaterials.TryGetValue(visualController.selectedSkin, out localGhostMaterial)) {
                    localGhostMaterial = new[] { new Material(Mod.GhostMaterial) };
                    GhostMaterials.Add(visualController.selectedSkin, localGhostMaterial);
                }
                if (!localGhostMaterial[0].mainTexture) {
                    localGhostMaterial[0].mainTexture = visualController.selectedSkin.texture;
                }
                visualController.UpdateMats(localGhostMaterial, localGhostMaterial);
                foreach (var c in Colliders) {
                    c.enabled = false;
                }
            }
        }
    }
}