﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace HideSelection {
    internal class EntityHideBehaviour : ManagedBehaviour {

        GenericEntity entity;
        EntityVisualController self;
        List<Collider> colliders;
        public bool IsHidden { get; private set; } = false;
        bool wasSimulating = false;

        void Start() {
            entity = GetComponent<GenericEntity>();
            self = entity.visualController;
            colliders = GetComponentsInChildren<Collider>(true).ToList();
            if (StatMaster.levelSimulating) {
                GameObject buildEntity = entity.entity.buildEntity.EntityBehaviour.gameObject;
                EntityHideBehaviour buildSelf = buildEntity.GetComponent<EntityHideBehaviour>();

                if (buildEntity.gameObject != this.gameObject && buildSelf.IsHidden) {
                    buildSelf.ChangeHiddenState(false, true);
                    List<Material> materials = buildEntity.GetComponentsInChildren<Renderer>(true).ToList().ConvertAll(x => x.material);
                    List<Renderer> simRenderers = GetComponentsInChildren<Renderer>(true).ToList();
                    for (int i = 0; i < materials.Count && i < simRenderers.Count; i++) {
                        simRenderers[i].material = materials[i];
                    }
                    colliders.ForEach(c => c.enabled = true);
                    buildSelf.ChangeHiddenState(true, true);
                    this.IsHidden = false;
                }

            }
        }

        protected override void OnUpdate() {
            if (StatMaster.levelSimulating != wasSimulating) {
                if (StatMaster.levelSimulating && IsHidden) {
                    ChangeHiddenState(false, true);
                }
                else {
                    ChangeHiddenState(IsHidden);
                }
            }
            wasSimulating = StatMaster.levelSimulating;
        }

        public void ChangeHiddenState(bool hidden, bool sim = false) {
            colliders = colliders.Where(c => c).ToList();
            if (!sim) {
                IsHidden = hidden;
            }
            if (!hidden) {
                self.Restore();
                colliders.ForEach(c => c.enabled = true);
            }
            else {
                if (!(entity is InsigniaTrigger)) {
                    self.ApplyMaterial(Mod.GhostMaterial);
                }
                colliders.ForEach(c => c.enabled = false);
            }
        }

    }
}
