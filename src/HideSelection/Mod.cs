using Modding;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace HideSelection {
    public class Mod : ModEntryPoint {

        public static ModKey HideKey;
        public static ModKey UnhideKey;

        public static Material GhostMaterial { get; private set; }

        public override void OnLoad() {
            HideButton.hideButtonTexture = ModResource.GetTexture("hide-icon");
            HideButton.unhideButtonTexture = ModResource.GetTexture("show-icon");

            HideKey = ModKeys.GetKey("hide-selection");
            UnhideKey = ModKeys.GetKey("unhide-selection");

            GameObject controller = GameObject.Find("ModControllerObject");
            if (!controller) { UnityEngine.Object.DontDestroyOnLoad(controller = new GameObject("ModControllerObject")); }
            controller.AddComponent<HideSelectionController>();
            controller.AddComponent<ManagedBehaviour.Manager>();

            GhostMaterial = new Material(ReferenceMaster.Instance.ghostShader);
            Color mpGhostColor = ReferenceMaster.Instance.mpGhostColor;
            float averageColorSingle = (mpGhostColor.r + mpGhostColor.g + mpGhostColor.b) / 3f;
            Color averageColor = new Color(averageColorSingle, averageColorSingle, averageColorSingle, 1f);
            GhostMaterial.SetColor("_GhostEmiss", Color.Lerp(mpGhostColor, averageColor, 0.5f) * 0.75f);
            GhostMaterial.SetColor("_GhostColor", mpGhostColor);

            Events.OnBlockInit += AddBlockHideController;
            Events.OnSurfaceCollidersGenated += OnSurfaceCollidersGenerated;
            Events.OnEntityPlaced += AddLevelHideController;
            Events.OnLevelLoaded += x => x.Entities.ToList().ForEach(AddLevelHideController);

            Events.OnActiveSceneChanged += (x, y) => OnSceneChanged();
            if (StatMaster.isMP) {
                OnSceneChanged();
            }

        }

        public static bool SceneNotPlayable() {
            return !AdvancedBlockEditor.Instance;
        }

        private void OnSurfaceCollidersGenerated(Modding.Blocks.Block obj) {
            obj.InternalObject.GetComponent<BlockHideBehaviour>()?.OnSurfaceCollidersChanged();
        }

        private void AddLevelHideController(Modding.Levels.Entity obj) {
            if (obj.InternalObject.EntityBehaviour.GetComponent<GoalMarker>()) {
                return;
            }
            obj.InternalObject.EntityBehaviour.gameObject.AddComponent<EntityHideBehaviour>();
        }

        private void AddBlockHideController(Modding.Blocks.Block obj) {
            if (obj.Machine.InternalObject == Machine.Active()
                && (obj.InternalObject.Prefab.ID != (int)BlockType.BuildNode)
                && (obj.InternalObject.Prefab.ID != (int)BlockType.BuildEdge)) {
                obj.InternalObject.gameObject.AddComponent<BlockHideBehaviour>();
            }
        }

        private void OnSceneChanged() {
            if (Mod.SceneNotPlayable()) { return; }
            if (!ModResource.AllResourcesLoaded) {
                ModResource.OnAllResourcesLoaded += OnSceneChanged;
                return;
            }

            Transform root = GameObject.Find("HUD").transform.FindChild("TopBar/Mover/Align (Middle)/Buttons");
            AddButtons(root.FindChild("Translate Tool").gameObject);
            AddButtons(root.FindChild("Rotate Tool").gameObject);

        }

        void AddButtons(GameObject go) {
            GameObject dupeButton = go.transform.FindChild("BG").GetChild(0).FindChild("dupe").GetChild(0).gameObject;
            dupeButton.transform.FindChild("BG (1)").localScale = new Vector3(0.29f, 0.482f, 1);
            dupeButton.transform.FindChild("BG").localScale = new Vector3(0.29f, 0.482f, 1);
            dupeButton.GetComponent<BoxCollider>().size = new Vector3(0.29f, 0.482f, 0.2f);
            dupeButton.transform.localPosition = new Vector3(-0.33f, -1.46f, 0);
            dupeButton.transform.FindChild("Icon").localScale *= 0.9f;

            GameObject hideButton = UnityEngine.Object.Instantiate(dupeButton, dupeButton.transform.parent, false) as GameObject;
            UnityEngine.Object.DestroyImmediate(hideButton.GetComponent<DuplicateButton>());
            hideButton.transform.FindChild("Icon").localScale *= 0.7f;
            hideButton.name = "Hide";
            hideButton.transform.localPosition = new Vector3(0, -1.46f, 0);

            GameObject unhideButton = UnityEngine.Object.Instantiate(hideButton, hideButton.transform.parent, false) as GameObject;
            unhideButton.name = "Unhide";
            unhideButton.transform.localPosition = new Vector3(0.33f, -1.46f, 0);

            hideButton.transform.parent.gameObject.AddComponent<ActivateOnSelection>().target = hideButton;
            unhideButton.transform.parent.gameObject.AddComponent<ActivateOnSelection>().target = unhideButton;
            hideButton.AddComponent<HideButton>();
            unhideButton.AddComponent<HideButton>().hideBlocks = false;
        }

        void FixTooltip(GameObject go) {
            Tooltip t = go.GetComponentInChildren<Tooltip>();
            Dictionary<Renderer, Color> renOrgColors = new Dictionary<Renderer, Color>();
            foreach (Renderer r in t.renOrgColors.Keys) {
                Color c = t.renOrgColors[r];
                c.a = 1f;
                renOrgColors[r] = c;
            }
            t.renOrgColors = renOrgColors;
            Dictionary<GameObject, Color> textOrgColors = new Dictionary<GameObject, Color>();
            foreach (GameObject g in t.textOrgColors.Keys) {
                Color c = t.textOrgColors[g];
                c.a = 1f;
                textOrgColors[g] = c;
            }
            t.textOrgColors = textOrgColors;
            //t.gameObject.GetComponentInChildren<TextMesh>().text = text;
        }
    }
}
