﻿using System;
using UnityEngine;
using System.Collections;

namespace HideSelection {
    public class UndoActionHide : UndoAction {

        bool hidden;

        public UndoActionHide(Machine m, Guid blockGuid, bool hidden) {
            machine = m;
            guid = blockGuid;
            this.hidden = hidden;
        }

        public override bool Redo() {
            machine.GetBlock(guid, out BlockBehaviour b);
            b.GetComponent<BlockHideBehaviour>().ChangeHiddenState(hidden);
            return true;
        }

        public override bool Undo() {
            machine.GetBlock(guid, out BlockBehaviour b);
            b.GetComponent<BlockHideBehaviour>().ChangeHiddenState(!hidden);
            return true;
        }
    }
}