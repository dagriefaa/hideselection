﻿using System.Collections.Generic;
using UnityEngine;

namespace HideSelection {
    internal class HideSelectionController : MonoBehaviour {

        void Update() {
            if (Mod.SceneNotPlayable() || ReferenceMaster.activeMachineSimulating || StatMaster.Mode.selectedTool == StatMaster.Tool.Modify) {
                return;
            }
            if (Mod.UnhideKey.IsDown) {
                SetHiddenStates(false);
            }
            else if (Mod.HideKey.IsDown) {
                SetHiddenStates(true);
            }
        }

        public static void SetHiddenStates(bool hidden) {
            if (AdvancedBlockEditor.Instance.selectionController.Selection.Count > 0) {
                List<UndoAction> actions = new List<UndoAction>();
                foreach (BlockBehaviour b in AdvancedBlockEditor.Instance.selectionController.MachineSelection) {
                    BlockHideBehaviour h = b.GetComponent<BlockHideBehaviour>();
                    if (h && h.IsHidden != hidden) {
                        h.ChangeHiddenState(hidden);
                        actions.Add(new UndoActionHide(Machine.Active(), b.Guid, hidden));
                    }
                }
                Machine.Active().UndoSystem.AddActions(actions);
            }
            else if (StatMaster.isMP && LevelEditor.Instance.selectionController.LevelSelection.Count > 0) {
                List<LevelUndoAction> levelActions = new List<LevelUndoAction>();
                foreach (LevelEntity e in LevelEditor.Instance.selectionController.LevelSelection) {
                    EntityHideBehaviour h = e.GetComponent<EntityHideBehaviour>();
                    if (h && h.IsHidden != hidden) {
                        h.ChangeHiddenState(hidden);
                        levelActions.Add(new LUAHideEntity(e, hidden));
                    }
                }
                LevelUndoSystem.Add(levelActions);
            }
        }
    }
}