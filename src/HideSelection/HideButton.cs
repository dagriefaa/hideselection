﻿using UnityEngine;
using System.Collections;

namespace HideSelection {
    public class HideButton : ClickBehaviour {

        public static Texture hideButtonTexture;
        public static Texture unhideButtonTexture;
        public bool hideBlocks = true;

        // Token: 0x06003FA4 RID: 16292 RVA: 0x00166200 File Offset: 0x00164400
        private void Awake() {
            bgRend = transform.FindChild("BG").GetComponent<Renderer>();
            bgRend.gameObject.SetActive(false);
            releaseOnlyOver = true;
        }

        void Start() {
            GetComponentInChildren<TextMesh>().text = hideBlocks ? "HIDE SELECTION" : "UNHIDE SELECTION";
            transform.FindChild("Icon").GetComponent<MeshRenderer>().material.mainTexture = hideBlocks ? hideButtonTexture : unhideButtonTexture;
        }

        // Token: 0x06003FA5 RID: 16293 RVA: 0x0016621C File Offset: 0x0016441C
        public override void OnClicked() {
            if (!enabled) {
                return;
            }
            Machine machine = Machine.Active();
            if (!machine || machine.isSimulating || !machine.CanModify) {
                return;
            }
            bgRend.gameObject.SetActive(true);
        }

        // Token: 0x06003FA6 RID: 16294 RVA: 0x00166270 File Offset: 0x00164470
        public override void OnClickReleased() {
            if (!enabled) {
                return;
            }
            HideSelectionController.SetHiddenStates(hideBlocks);
            bgRend.gameObject.SetActive(false);
        }

        // Token: 0x06003FA7 RID: 16295 RVA: 0x001662E8 File Offset: 0x001644E8
        protected void OnMouseExit() {
            if (releaseOnlyOver) {
                bgRend.gameObject.SetActive(false);
            }
        }

        // Token: 0x04003A83 RID: 14979
        public Renderer bgRend;
    }
}