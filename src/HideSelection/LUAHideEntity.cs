﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HideSelection {
    class LUAHideEntity: LevelUndoAction {
        bool hidden;
        public LUAHideEntity(LevelEntity e, bool hidden): base(e) {
            this.hidden = hidden;
        }

        public override void Redo() {
            entity?.behaviour.GetComponent<EntityHideBehaviour>().ChangeHiddenState(hidden);
        }

        public override void Undo() {
            entity?.behaviour.GetComponent<EntityHideBehaviour>().ChangeHiddenState(!hidden);
        }
    }
}
