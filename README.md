<div align="center">
<img src="header.png" alt="Additive Loading" width="500"/><br>
https://steamcommunity.com/sharedfiles/filedetails/?id=1943105719<br>
A mod for [Besiege](https://store.steampowered.com/app/346010), a physics based building sandbox game. <br><br>

![Steam Views](https://img.shields.io/steam/views/1943105719?color=%231b2838&logo=steam&style=for-the-badge)
![Steam Subscriptions](https://img.shields.io/steam/subscriptions/1943105719?color=%231b2838&logo=steam&style=for-the-badge)
![Steam Favorites](https://img.shields.io/steam/favorites/1943105719?color=%231b2838&logo=steam&style=for-the-badge)
</div>

Selection Hiding is a mod for [Besiege](https://store.steampowered.com/app/346010) which approximates v0.45-era Building Tools selection hiding functionality. It lets you see and click through selected blocks (and level entities) in build mode without deleting or moving them (which reduces errors in building).

This is especially helpful when dealing with giant level entities and their horrible selection behaviour.

<div align="center"><img src="https://i.imgur.com/8R68hOl.png" width="500" /></div>

## Technologies
* Unity 5.4 - the game engine Besiege runs on
* C# 3.0 - the scripting language that Unity 5.4 shipped with

## Installation
The latest version of the mod can be found on the Steam Workshop, and can be installed at the press of the big green 'Subscribe' button. It will automatically activate the next time you boot the game.

If, for whatever reason, you don't want to use Steam to manage your mods, installation is as follows:
1. Install Besiege.
2. Clone the repository into `Besiege/Besiege_Data/Mods`.
3. Open the `.sln` file in `src` in Visual Studio 2015 (or later).
4. Press F6 to compile the mod.

## Usage
Usage instructions can also be found on the [Steam Workshop page](https://steamcommunity.com/sharedfiles/filedetails/?id=1943105719).

In short:
* Select blocks or entities.
* Press Ctrl+H to hide them, and Alt+H to unhide them. For blocks, there are also hide/unhide buttons underneath the tool buttons.
* Ctrl+A or drag selecting is helpful for selecting hidden things, since they obviously can't be selected by clicking on them.

## Status
This mod is feature-complete. <br>
However, additional features and fixes may be added in the future as necessary.
